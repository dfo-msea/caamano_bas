# caamano_BAS

Code by Patrick Thompson  
patrick.thompson@dfo-mpo.gc.ca  
Project lead - Emily Rubidge  
emily.rubidge@dfo-mpo.gc.ca  

Script to produce Balance Acceptance Sample for electing sites to sample eDNA in Caamano Sound. 
